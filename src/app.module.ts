import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AnimalModule } from './animal/animal.module';
import { Animal } from './animal/entities/animal.entity';

@Module({
  imports: [
    AnimalModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'host.docker.internal', //localhost-local or mysql-docker
      port: 3306,
      username: 'root',
      password: 'secret',
      database: 'animal',
      entities: [Animal],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
